from django.contrib import admin

# Register your models here.

from .models import Kegiatan, DataRegistran

admin.site.register(Kegiatan)
admin.site.register(DataRegistran)
