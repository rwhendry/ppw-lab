from django.shortcuts import render, redirect
from django.http import JsonResponse
from .models import Kegiatan, DataRegistran

def index(request):
    return render(request, 'index.html')

def organization(request):
    return render(request, 'organization.html')

def register(request):
    dataregistran = DataRegistran.objects.all()
    context = {"dataregistran" : dataregistran}
    return render(request, 'register.html', context)

def kegiatan(request):
    if request.method == 'POST':
        nama, tempat, waktu, jam = request.POST.get('nama'), request.POST.get('tempat'), request.POST.get('waktu'), request.POST.get('jam')
        kegiatan =Kegiatan()
        try:
            kegiatan.namakegiatan = nama
            kegiatan.tempat = tempat
            kegiatan.waktu = waktu +  " " + jam + ":00"
            kegiatan.save()
            return redirect("/listkegiatan")
        except:
            context = {'error' : True};
            return render(request, 'kegiatan.html', context)
    return render(request, 'kegiatan.html')

def list_kegiatan(request):
    list_kegiatan_all = Kegiatan.objects.all()
    context = {'list_kegiatan' : list_kegiatan_all}
    return render(request, 'listkegiatan.html', context)

def remove_all(request):
    if request.method == 'POST':
        Kegiatan.objects.all().delete()
    return redirect("/listkegiatan")


def checkvalidasi(request):
    search = request.GET.get('email', "")

    dataregistran = DataRegistran()

    data = DataRegistran.objects.filter(email = search).exists()

    return JsonResponse({'data' : data})

def submit(request) :
    nama1 = request.POST.get("name")
    password1 = request.POST.get("password")
    email1 = request.POST.get("email")

    if(DataRegistran.objects.filter(email = email1).exists()) :
        return JsonResponse({'status' : 200})

    dataregistran = DataRegistran(email = email1, nama = nama1, password = password1)

    dataregistran.save()

    return JsonResponse({'status' : 200})


def delete(request):

    password1 = request.POST.get("password")
    email1 = request.POST.get("email")

    print(password1)

    dataregistran = DataRegistran.objects.filter(email = email1)[0]


    if(dataregistran.password != password1):
        return JsonResponse({'status' : 404})
    else:
        dataregistran.delete()
        return JsonResponse({'status' : 200})

def list_data(request):

    dataregistran = list(DataRegistran.objects.all().values())

    return JsonResponse({'data' : dataregistran})
