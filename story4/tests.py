from django.test import TestCase
from .models import DataRegistran

def test_add_dataregistran(self):
    dataregistran = DataRegistran(email="wijaya@gmail.com", nama = "saya", password = "soalnyalogampangparahcok")
    dataregistran.save()

    database_count = DataRegistran.objects.all().count()
    self.assertEqual(database_count, 1)
