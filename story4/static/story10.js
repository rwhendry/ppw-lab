var activeButton = false;


id = 1;

function validate() {

    key = $("#email-box").val()
    $.ajax(
    {
      url : '/checkvalidasi',
      method: 'GET',
      data: {
        email : key
      },
      success: function(result){
          const item = String(result["data"])
          if(item === "false") {
            $("#email-valid").html("")
            activateButton()
          }
          else {
            $("#email-valid").html("Email tersebut sudah pernah didaftarkan sebelumnya, silakan daftar dengan email lain")
            deactiveButton()
          }
      }
    });
}

function activateButton() {
  $("#registrasi").removeAttr("disabled")
}

function deactiveButton() {
  $("#registrasi").attr("disabled", true)
}


$("#registrasi").on('click',function(){
  $.ajax(
  {
    url: '/submit',
      method: 'POST',
      data: {
        name: $('#name').val(),
        email: $('#email-box').val(),
        password: $('#password').val(),
        csrfmiddlewaretoken: $('[name=csrfmiddlewaretoken]').val()
      },
      success: function(result){
        alert("Data berhasil disimpan")
        addlist()
      }
  });
});


function addlist(email) {

  $.ajax(
    {
        url: '/listdata',
        method: 'GET',
        success: function(result){
          text = ""
          const data = result["data"];
          for(let i = 0; i < data.length; i++){
            let id = data[i]["id"]
            let email = data[i]["email"]
            text = text +
            `
            <div id="list`+id+`">
              <h3 id="email`+id+`">`+email+`</h3>
              <div id="div`+id+`">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal`+id+`">
                  Unsuscribe
                </button>
              </div>
            </div>
            <div class="modal fade" id="modal`+id+`" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-body font-black">
                    Do you really want to unsuscribe? Input you password below to confirmate.
                    <input id="passwordconfirmation`+id+`" type="password"> </input>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick=unsub(`+id+`) data-dismiss="modal">Are you sure</button>
                  </div>
                </div>
              </div>
            </div>`
          }
          $("#list-data").empty()
          $("#list-data").append(text)
        }
    }
  )
}

function unsub(id){
  console.log($('#email'+id).html())
  $.ajax(
    {
      url: '/delete',
        method: 'POST',
        data: {
          email: $('#email'+id).html(),
          password: $('#passwordconfirmation'+id).val(),
          csrfmiddlewaretoken: $('[name=csrfmiddlewaretoken]').val()
        },
        success: function(result){
          const status = String(result["status"])
          if(status == "404") {
            alert("Password salah")
          }
          else{
            alert("Data berhasil dihapus")
            $("#list"+id).remove("")
          }
        }
    }
  )
}
