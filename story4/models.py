from django.db import models

# Create your models here.

class Kegiatan(models.Model):
    waktu = models.DateTimeField()
    namakegiatan = models.CharField(max_length = 200)
    tempat = models.CharField(max_length = 200)

    def __str__(self):
        return self.namakegiatan


class DataRegistran(models.Model):
    email = models.CharField(max_length = 200)
    nama = models.CharField(max_length = 200)
    password = models.CharField(max_length = 200)
