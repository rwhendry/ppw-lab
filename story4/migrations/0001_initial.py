# Generated by Django 2.1.1 on 2018-10-03 00:49

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Kegiatan',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('waktu', models.DateTimeField()),
                ('namakegiatan', models.CharField(max_length=200)),
                ('tempat', models.CharField(max_length=200)),
            ],
        ),
    ]
