"""ppw URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import re_path
from .views import index, register, organization, kegiatan, list_kegiatan, remove_all, checkvalidasi, submit, delete, list_data

urlpatterns = [
    re_path(r'^$', index),
    re_path(r'^register/$', register),
    re_path(r'^organization/$', organization),
    re_path(r'^kegiatan', kegiatan),
    re_path(r'^listkegiatan/$',list_kegiatan),
    re_path(r'^removeall',remove_all),
    re_path(r'^checkvalidasi$',checkvalidasi),
    re_path(r'^submit', submit),
    re_path(r'^delete', delete),
    re_path(r'^listdata', list_data)
]
